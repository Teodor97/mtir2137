package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerTest {

    private AppController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new AppController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void createTest_PC01_F02() {
        try{
            ctrl.createNewTest();
        }catch (NotAbleToCreateTestException ex)  {
            assert true;
        }
    }

    @Test
    public void createTest_PC02_F02() {
        for(int i=0;i<5;i++) {
            try {
                Intrebare intrebare = new Intrebare("C"+i+"?","1)"+i,"2)"+i,"3)"+i,"2","Masini");
                ctrl.addNewIntrebare(intrebare);
            } catch (InputValidationFailedException | DuplicateIntrebareException e) {
                e.printStackTrace();
            }
        }
        try{
            ctrl.createNewTest();
        }catch (NotAbleToCreateTestException ex)  {
            assert true;
        }
    }

    @Test
    public void createTest_PC03_F02() {
        evaluator.model.Test test = new evaluator.model.Test();
        for(int i=0;i<5;i++) {
            try {
                Intrebare intrebare = new Intrebare("C"+i+"?","1)"+i,"2)"+i,"3)"+i,"2","Masini"+i);
                ctrl.addNewIntrebare(intrebare);
            } catch (InputValidationFailedException | DuplicateIntrebareException e) {
                e.printStackTrace();
            }
        }
        try{
            test = ctrl.createNewTest();
        }catch (NotAbleToCreateTestException ex){
            ex.printStackTrace();
        }
        assertEquals(test.getIntrebari().size(),5);
    }

    @Test
    public void createTest_DC01_F03(){
        try{
            Statistica statistica = ctrl.getStatistica();
        } catch (NotAbleToCreateStatisticsException e) {
            assert true;
        }
    }

    @Test
    public void createTest_DC02_F03(){
        Statistica statistica = new Statistica();
        try{
            Intrebare intrebare = new Intrebare("Ce este?","1)a","2)b","3)c","2","Scoala");
            ctrl.addNewIntrebare(intrebare);
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
        try{
            statistica = ctrl.getStatistica();
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
        assertEquals(statistica.getIntrebariDomenii().size(),1);
    }
}
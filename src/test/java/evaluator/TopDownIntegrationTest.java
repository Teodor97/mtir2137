package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TopDownIntegrationTest {

    private AppController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new AppController();
    }

    @Test
    public void test_F01(){
        //cerinta is more than 2 character and less than 50
        try{
            Intrebare intrebare = new Intrebare("C?","1)ceva","2)ceva","3)ceva","2","Maimute");
            ctrl.addNewIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
            assert false;
        } catch (DuplicateIntrebareException e) {
            e.printStackTrace();
        } finally {
            assert true;
        }
    }

    @Test
    public void test_F02(){
        evaluator.model.Test test = new evaluator.model.Test();
        for(int i=0;i<5;i++) {
            try {
                Intrebare intrebare = new Intrebare("C"+i+"?","1)"+i,"2)"+i,"3)"+i,"2","Masini"+i);
                ctrl.addNewIntrebare(intrebare);
            } catch (InputValidationFailedException | DuplicateIntrebareException e) {
                e.printStackTrace();
            }
        }
        try{
            test = ctrl.createNewTest();
        }catch (NotAbleToCreateTestException ex){
            ex.printStackTrace();
        }
        assertEquals(test.getIntrebari().size(),5);
    }

    @Test
    public void test_F03(){
        Statistica statistica = new Statistica();
        try{
            Intrebare intrebare = new Intrebare("Ce este?","1)a","2)b","3)c","2","Scoala");
            ctrl.addNewIntrebare(intrebare);
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            e.printStackTrace();
        }
        try{
            statistica = ctrl.getStatistica();
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
        assertEquals(statistica.getIntrebariDomenii().size(),1);
    }

    @Test
    public void test_TopDown01(){
        ctrl.loadIntrebariFromFile("C:\\Users\\Teodor\\Desktop\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        try {
            ctrl.addNewIntrebare(new Intrebare("Ce este?","1)a","2)b","3)c","3","Da"));
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            assert false;
        }
    }

    @Test
    public void test_TopDown02(){
        ctrl.loadIntrebariFromFile("C:\\Users\\Teodor\\Desktop\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt");
        evaluator.model.Test test = new evaluator.model.Test();
        try {
            test = ctrl.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
        }
        assertEquals(test.getIntrebari().size(),5);
        try {
            ctrl.addNewIntrebare(new Intrebare("Ce este?","1)a","2)b","3)c","3","Da"));
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            assert false;
        }
    }

    @Test
    public void test_TopDown03(){
        for(int i=0;i<5;i++) {
            try {
                Intrebare intrebare = new Intrebare("C"+i+"?","1)"+i,"2)"+i,"3)"+i,"2","Masini"+i);
                ctrl.addNewIntrebare(intrebare);
            } catch (InputValidationFailedException | DuplicateIntrebareException e) {
                e.printStackTrace();
            }
        }
        evaluator.model.Test test = new evaluator.model.Test();
        try {
            test = ctrl.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            e.printStackTrace();
        }
        assertEquals(test.getIntrebari().size(),5);
        Statistica statistica = new Statistica();
        try {
            statistica = ctrl.getStatistica();
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }
        assertEquals(statistica.getIntrebariDomenii().size(),5);
        try {
            ctrl.addNewIntrebare(new Intrebare("Ce este?","1)a","2)b","3)c","3","Da"));
        } catch (DuplicateIntrebareException | InputValidationFailedException e) {
            assert false;
        }
    }

}

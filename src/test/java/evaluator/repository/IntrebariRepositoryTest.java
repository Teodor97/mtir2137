package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTest {

    IntrebariRepository repo;

    @Before
    public void setUp() throws Exception {
        repo = new IntrebariRepository();

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void addIntrebareECP1() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta starts with upercase and ends in "?"
        Intrebare intrebare = new Intrebare("Ce zici?","1)ceva","2)ceva","3)ceva","3","Maimute");
        repo.addIntrebare(intrebare);
        assertEquals(repo.getIntrebari().size(),1);
    }

    @Test
    public void addIntrebareECP2() throws InputValidationFailedException, DuplicateIntrebareException {
        //variantaCorecta is "1","2" or "3"
        try{
            Intrebare intrebare = new Intrebare("Ce zici?","1)ceva","2)ceva","3)ceva","4","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),0);
        }

    }

    @Test
    public void addIntrebareECP3() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta starts with upercase and ends in "?"
        try{
            Intrebare intrebare = new Intrebare("ce zici?","1)ceva","2)ceva","3)ceva","3","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),0);
        }
    }

    @Test
    public void addIntrebareECP4() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta starts with upercase and ends in "?"
        try{
            Intrebare intrebare = new Intrebare("Ce zici","1)ceva","2)ceva","3)ceva","3","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),0);
        }
    }

    @Test
    public void addIntrebareBVA1() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta is more than 2 character and less than 50
        try{
            Intrebare intrebare = new Intrebare("C?","1)ceva","2)ceva","3)ceva","2","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),1);
        }
    }



    @Test
    public void addIntrebareBVA2() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta is more than 2 character and less than 50
        try{
            Intrebare intrebare = new Intrebare("Caaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa?","1)ceva","2)ceva","3)ceva","2","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),0);
        }
    }

    @Test
    public void addIntrebareBVA3() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta is more than 2 character and less than 50
        try{
            Intrebare intrebare = new Intrebare("Caaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa?","1)ceva","2)ceva","3)ceva","2","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),1);
        }
    }

    @Test
    public void addIntrebareBVA4() throws InputValidationFailedException, DuplicateIntrebareException {
        //cerinta is more than 2 character and less than 50
        try{
            Intrebare intrebare = new Intrebare("?","1)ceva","2)ceva","3)ceva","2","Maimute");
            repo.addIntrebare(intrebare);
        }catch (InputValidationFailedException ex){
        }finally {
            assertEquals(repo.getIntrebari().size(),0);
        }
    }

}
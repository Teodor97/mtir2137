package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "C:\\Users\\Teodor\\Desktop\\5-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		appController.loadIntrebariFromFile(file);

		boolean activ = true;
		String optiune = null;
		String varianta1,varianta2,varianta3,enunt,domeniu,variantaCorecta;
		Test test;
		Statistica statistica;
		Intrebare intrebare;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				System.out.println("Da-ti enuntul problemei:");
				enunt = console.readLine();
				System.out.println("Da-ti prima varianta de raspuns:");
				varianta1 = console.readLine();
				System.out.println("Da-ti a doua varianta de raspuns:");
				varianta2 = console.readLine();
				System.out.println("Da-ti a treia varianta de raspuns:");
				varianta3 = console.readLine();
				System.out.println("Da-ti varianta corecta de raspuns:");
				variantaCorecta = console.readLine();
				System.out.println("Da-ti domeniul problemei:");
				domeniu = console.readLine();
				try{
					intrebare = new Intrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
					appController.addNewIntrebare(intrebare);
				} catch (InputValidationFailedException | DuplicateIntrebareException e) {
					System.out.println(e);
				}

				break;
			case "2" :
				try{
					test = appController.createNewTest();
				} catch (NotAbleToCreateTestException e) {
					System.out.println(e);
				}
				break;
			case "3" :
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					System.out.println(e);
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
